/*******************************************************************************

NAME            iserArpIcmp.h
SUMMARY         %description%
VERSION         %version: 6 %
UPDATE DATE     %date_modified: Thu Apr  3 13:18:11 2014 %
PROGRAMMER      %created_by:    lgranrud %

        Copyright 2014 NetApp, Inc. All Rights Reserved.

DESCRIPTION:

INCLUDE FILES:

NOTES:

RESTRICTIONS:

SEE ALSO:

REFERENCE:

IMPLEMENTATION:

MODIFICATION HISTORY:

*******************************************************************************/


#ifndef __INCiserArpIcmp
#define __INCiserArpIcmp


/***  INCLUDES  ***/
#include "iserDefs.h"
#include "iserMtlInterface.h"
#include "iserMtlIoc.h"
#include "iserWorkqueue.h"
#include "iserRdmaVerbs.h"
#include "utlShowableObject.h"

#include "linux/if_infiniband.h"
#include "rdma/rdma_cm.h"


/***  CONSTANT DEFINITIONS  ***/


/***  MACRO DEFINITIONS  ***/


/***  TYPE DEFINITIONS  ***/


#define IP_ADDR_LEN         4
#define ARP_FRAME_TYPE  0x0806
#define IPv4_FRAME_TYPE 0x0800
#define IPv6_FRAME_TYPE 0x086DD
#define IB_HW_TYPE      0x20
#define IB_HW_ADDR_LEN  16
#define IP_PROTO_TYPE   0x0800
#define QPN_MASK        0xFFFFFF

#define PROTOCOL_ICMP   0x01
#define PROTOCOL_TCP    0x06

#define ISER_ARP_TIMEOUT    300        // ARP Time-out in ticks (5 seconds)
#define ARPOP_REQUEST       1
#define ARPOP_REPLY         2

// Total simultaneous requests
#define ARP_ICMP_NUM_BUF    16
#define ARP_REQ_NUM_BUF     1    // Max number of outstanding ARP Requests
#define ARP_ICMP_TOTAL_NUM_BUF  (ARP_ICMP_NUM_BUF + ARP_REQ_NUM_BUF)
#define ARP_BUFFER_SIZE     256  // Larger than ARP + GRH


struct FrameHdr
{
    UINT16          frameType;
    UINT16          rcv;
} __attribute__((packed));

/* represents the structure of an IB ARP packet */
struct IPv4ArpPacket
{
    struct FrameHdr header;
    UINT16          hwType;
    UINT16          protType;
    BYTE            hwAddrSize;
    BYTE            protAddrSize;
    UINT16          op;
    UINT32          sourceQPN;
    BYTE            sourceGID[IB_HW_ADDR_LEN];
    BYTE            senderIpAddr[IPv4_LENGTH];
    UINT32          targetQPN;
    BYTE            targetGID[IB_HW_ADDR_LEN];
    BYTE            targetIpAddr[IPv4_LENGTH];
} __attribute__((packed));

struct IPv6ArpPacket
{
    struct FrameHdr header;
    UINT16          hwType;
    UINT16          protType;
    BYTE            hwAddrSize;
    BYTE            protAddrSize;
    UINT16          op;
    UINT32          sourceQPN;
    BYTE            sourceGID[IB_HW_ADDR_LEN];
    BYTE            senderIpAddr[IPv6_LENGTH];
    UINT32          targetQPN;
    BYTE            targetGID[IB_HW_ADDR_LEN];
    BYTE            targetIpAddr[IPv6_LENGTH];
} __attribute__((packed));

struct IPv4Packet
{
    BYTE            versionLength;      // 4,4
    BYTE            dscpEcn;            // 4,4
    UINT16          totalLength;
    UINT16          identification;     //
    UINT16          flagsFragment;      //  3,13
    BYTE            ttl;
    BYTE            protocol;
    UINT16          checksum;           // Header checksum
    UINT32          sourceAddr;
    UINT32          destAddr;
    // Options follow if versionLength & 0xf > 5
} __attribute__((packed));

struct TCPPacket
{
    UINT16          sourcePort;
    UINT16          destPort;
    UINT32          seqNo;
    UINT32          ackNo;
    UINT16          options;
    #define     DATA_OFFSET         0xf000
    #define     DATA_OFFSET_SHIFT   12
    #define     TCP_NS              0x100
    #define     TCP_CWR             0x080
    #define     TCP_ECE             0x040
    #define     TCP_URG             0x020
    #define     TCP_ACK             0x010
    #define     TCP_PSH             0x008
    #define     TCP_RST             0x004
    #define     TCP_SYN             0x002
    #define     TCP_FIN             0x001
    UINT16          windowSize;
    UINT16          checksum;
    UINT16          urgentPtr;
    // Options follow if versionLength & 0xf > 5
} __attribute__((packed));

struct TCPPseudoHdr
{
    UINT32          sourceAddr;
    UINT32          destAddr;
    BYTE            zero;
    BYTE            protocol;
    UINT16          totalLength;
} __attribute__((packed));

struct IcmpHdr
{
    BYTE            icmpType;
    #define         ECHO_REQUEST    8
    #define         ECHO_REPLY      0
    BYTE            code;
    UINT16          checksum;   // Ones complement checksum of header plus data.
    UINT16          id;         // Only defined for echo
    UINT16          sequence;   // Only defined for echo
    // Payload follows BYTE payload [unknown]
} __attribute__((packed));

/***  GLOBALS  ***/

namespace iser
{

    class MtlInterface;
    class NetDevice;
    class ArpIcmp;

    static const UINT32     McastMaxDelay=16;       // Max Mcast retry

    /// typedef for the work callback
    typedef void (ArpIcmp::*ArpIcmpWork)();

    /**
     * \brief ArpWork is the definition of a method to be called on
     *        the Workqueue task.  Basically it is used to change
     *        from an interrupt or Event context to a task context.
     *        Events are task, but they are presented with
     *        semaphores locked.
     *
     */
    class ArpWork : public Work
    {
    public:
        ArpWork(Workqueue * workQ, ArpIcmp * data, ArpIcmpWork func);
        ~ArpWork();

        void        doWork();

        const char* toString() const;

    private:
        ArpIcmp *           m_Class;
        ArpIcmpWork         m_WorkMethod;
    };

    /**
     *  \brief ArpDelayed Work is the definition of a method to be
     *          called on the Workqueue task after a delay.
     *          Basically it is used to change from an interrupt or
     *          Event context to a task context. Events are task,
     *           but they are presented with semaphores locked.
     *
     */
    class ArpDelayedWork : public DelayedWork
    {
    public:
        ArpDelayedWork(Workqueue * workQ, ArpIcmp * data, ArpIcmpWork func);

        virtual ~ArpDelayedWork();

        void        doWork();

        const char* toString() const;

    private:
        ArpIcmp *           m_Class;
        ArpIcmpWork         m_WorkMethod;
    };


    /**
     * \brief ARP and ICMP methods
     *
     */
    class ArpIcmp : public utl::ShowableObject
    {
    public:
        ArpIcmp (MtlInterface * intf, NetDevice * netDevice );
        virtual ~ArpIcmp();

        // from utl::ShowableObject
        const char*                 toStringHeading(int level=0) const { return "NA"; }
        const char*                 toString(int level=0)        const { return "iserArpImcp"; }
        void                        showDetails()                const;

        bool                        arpStart();

        //  Callback routine -> C++ calls
        int                         cycleQPState();
        void                        receiveCompletionCallback(struct ib_cq *cq);
        void                        sendCompletionCallback(struct ib_cq *cq);
        void                        eventHandler(struct ib_event *event);

        bool                        sendArpRequest(BYTE * addr, UINT32 * remoteQPN, BYTE * remoteGID);
        bool                        sendArpV6Request(BYTE * addr, UINT32 * remoteQPN, BYTE * remoteGID);
        static ArpIcmp*             getArpIcmpFromEventHandler(struct ib_event_handler* handler);

    private:
        inline MtlInterface *       getIntf()               { return m_Intf; }
        inline struct ib_device *   getIbDevice()           { return m_Intf->getIbDevice(); }
        inline BYTE                 getIbPortNumber() const { return m_Intf->getIbPortNumber(); }
        inline unsigned             getChannelNumber()const { return m_Intf->getChannelNumber(); }


        bool                        arpCreateQP();
        void                        arpCleanupQP();

        bool                        arpFrame(struct ib_wc * wc, BYTE * pktStart, UINT32 pktLength);

        void                        arpIPv4CreateResponse(struct IPv4ArpPacket * arpPkt,
                                                          BYTE ipAddr[IPv4_LENGTH]);
        void                        arpIPv6CreateResponse(struct IPv6ArpPacket * arpPkt,
                                                          BYTE ipAddr[IPv6_LENGTH]);
        void                        arpIPv4CreateRequest(BYTE destIpAddr[IPv4_LENGTH],
                                                         struct IPv4ArpPacket * arpPkt);
        void                        arpIPv6CreateRequest(BYTE destIpAddr[IPv6_LENGTH],
                                                         struct IPv6ArpPacket * arpPkt);

        bool                        arpMulticastInit();

        void                        createIPv4BroadcastGID(union ib_gid *mgid);
        inline UINT16               checksum(UINT16 * buf, UINT32 length);
        bool                        isIPv4ArpRequest(struct IPv4ArpPacket * ib_pkt,
                                                     BYTE * ipAddr);
        bool                        isIPv6ArpRequest(struct IPv6ArpPacket * ib_pkt,
                                                     BYTE * ipAddr);
        bool                        isIPv4ArpReply(struct IPv4ArpPacket * ib_pkt,
                                                   BYTE * ipAddr, BYTE * senderIpAddr);
        bool                        isIPv6ArpReply(struct IPv6ArpPacket * ib_pkt,
                                                   BYTE * ipAddr, BYTE * senderIpAddr);
        struct ib_ah *              create_ah_for_arp();
        bool                        postReceive(int idx);
        bool                        postSend(int idx, void * buf, int length, UINT32 remoteQP);

        //
        //  Multicast retry
        void                        mcastTimerStart(unsigned long delay);
        void                        mcastTimerStop();
        void                        mcastTimerEnable();
        void                        mcastTimerDisable();
        void                        mcastRetry();

        //
        //  Event processing
        void                        SMChanged();
        void                        PortChanged();
        void                        PkeyChanged();

        MtlInterface *              m_Intf;             // MtlInterface pointer
        NetDevice *                 m_NetDevice;        // NetDevice to check addresses
        struct ib_qp *              m_QP;               // Queue Pair
        struct ib_pd *              m_PD;               // Protection domain
        struct ib_cq *              m_RxCQ;             // Receive Completion queue
        struct ib_cq *              m_TxCQ;             // Transmit Completion queue
        struct ib_mr *              m_MR;               // Memory Region
        UINT32                      m_QKey;             // Host order QKey
        BYTE *                      m_Buffer;           // buffer ARP_ICMP_TOTAL_NUM_BUF*ARP_BUFFER_SIZE
        BYTE *                      m_DataBuf[ARP_ICMP_TOTAL_NUM_BUF];
        struct ib_ah *              m_Ah[ARP_ICMP_TOTAL_NUM_BUF];

        BYTE                        m_McastDelay;       // current delay in seconds
        UINT32                      m_McastRetry;       // Current retry counter
        ArpDelayedWork              m_McastDelayWork;   // Work request
                                                        //
        ArpWork                     m_SMChanged;        // Work request for SM_CHANGE/CLIENT_REREGISTER
        ArpWork                     m_PortChanged;      // Work request for PORT_ERR/PORT_ACTIVE/LID_CHANGE
        ArpWork                     m_PkeyChanged;      // Work request for PKEY Changed
        bool                        m_McastTimerRunning;    // We are waiting to try mcast again
        bool                        m_McastTimerEnabled;    // Multicast timer is available to start
        bool                        m_McastJoinBusy;

        bool                        m_MulticastAttached;    // ib_detach_mcast needed
        struct ib_event_handler     m_ArpEventHandler;

        // Members used for sending ARP requests
        BYTE                        m_ActiveIPv4Addr[IPv4_LENGTH];
        BYTE                        m_ActiveIPv6Addr[IPv6_LENGTH];
        SEM_ID                      m_ActiveArpRequest;
        int                         m_ArpRequestIdx;
        UINT32                      m_remoteQPN;
        BYTE                        m_remoteGID[IB_HW_ADDR_LEN];

        struct ib_flow *            m_flow;
    };
}

/**
 * Compute the one complement checksum of buffer.
 *
 * Buffer MUST be aligned on a 2 byte boundary.  The caller is
 * responsible for making sure that is true.
 *
 *
 * \param buf       Buffer
 * \param length    Length in BYTES
 *
 * \return UINT16   Compute checksum
 */
inline UINT16
iser::ArpIcmp::checksum
    (
    UINT16 * buf,
    UINT32 length
    )
{
    UINT32      checksum = 0;

    for (UINT32 i = 0; i < length/2; i++)
    {
        checksum += buf[i];
    }
    //  Handle overflows
    checksum = (checksum & 0xFFFF) + ((checksum >> 16) & 0xFFFF);
    checksum = (checksum & 0xFFFF) + ((checksum >> 16) & 0xFFFF); // handle second level overflows

    return ~(UINT16)checksum;       // One's complement
}




#endif        /* End of __INCiserArpIcmp */
