/*******************************************************************************

NAME            iserArpIcmp.cc
SUMMARY         %description%
VERSION         %version: 8 %
UPDATE DATE     %date_modified: Wed Apr 16 10:12:12 2014 %
PROGRAMMER      %created_by:    schad %

        Copyright 2014 NetApp, Inc. All Rights Reserved.

DESCRIPTION:


*******************************************************************************/

/***  INCLUDES  ***/

#include "vkiWrap.h"
#include "iserDefs.h"
#include "iserArpIcmp.h"
#include "linux/err.h"
#include "utlOffset.h"

extern DqWriter *iserDqWriter;
extern DqWriter* iserIoDqWriter;
static USHORT iserFstrBase;
static USHORT iserIoFstrBase;

/*
 *  iserArpDqBufLength controls how much of a buffer is saved in the DQ.
 *  In production I would expect this to be 0.  It is higher for current
 *  debug.
 */
int iserArpDqBufLength = 128;

/***  CONSTANT DEFINITIONS  ***/


/***  MACRO DEFINITIONS  ***/

// some header keeps undefining this
#if !defined(min)
    #define min(x, y)   (((x) < (y)) ? (x) : (y))
#endif


/***  TYPE DEFINITIONS  ***/

#define ISER_ARP_START                      (iserFstrBase + 0)
#define ISER_ARP_PING_NOT_ATTACHED          (iserFstrBase + 1)
#define ISER_ARP_PINGV6_NOT_ATTACHED        (iserFstrBase + 2)
#define ISER_CREATE_QP                      (iserFstrBase + 3)
#define ISER_CLEANUP_QP                     (iserFstrBase + 4)
#define ISER_POST_RECEIVE_FAILED            (iserFstrBase + 5)
#define ISER_POST_SEND_ERROR                (iserFstrBase + 6)
#define ISER_RECEIVE_BUF                    (iserFstrBase + 7)
#define ISER_SEND_BUF                       (iserFstrBase + 8)
#define ISER_SEND_COMP_ERROR                (iserFstrBase + 9)
#define ISER_MULT_ATTACH                    (iserFstrBase + 10)
#define ISER_MULT_JOIN_ERROR                (iserFstrBase + 11)
#define ISER_MULT_JOIN_RET                  (iserFstrBase + 12)
#define ISER_MULT_JOIN_COMP                 (iserFstrBase + 13)
#define ISER_QP_MODIFY_ERROR                (iserFstrBase + 14)
#define ISER_QP_STATE                       (iserFstrBase + 16)
#define ISER_QP_READY                       (iserFstrBase + 17)
#define ISER_ATTACH_MULTICAST_ERROR         (iserFstrBase + 18)
#define ISER_TIMER_START                    (iserFstrBase + 19)
#define ISER_ARPICMP_SM_CHG                 (iserFstrBase + 20)
#define ISER_ARPICMP_PORT_CHG               (iserFstrBase + 21)
#define ISER_ARPICMP_PORT_CHG_QUEUE         (iserFstrBase + 22)
#define ISER_ARPICMP_PKEY_CHG               (iserFstrBase + 23)
#define ISER_ARP_START_QUEUE                (iserFstrBase + 24)
#define ISER_MULT_JOIN_RETRY                (iserFstrBase + 25)
#define ISER_ARPICMP_EVENT                  (iserFstrBase + 26)
#define ISER_TIMER_STOP                     (iserFstrBase + 27)
#define ISER_TIMER_ENABLE                   (iserFstrBase + 28)
#define ISER_TIMER_DISABLE                  (iserFstrBase + 29)
#define ISER_CLEANUP_QP_DONE                (iserFstrBase + 30)

#define ISER_RDMA_BIND_ADDR_FAILED          (iserFstrBase + 31)
#define ISER_RDMA_RSVL_ADDR_FAILED          (iserFstrBase + 32)
#define ISER_RDMA_RSVL_ROUT_FAILED          (iserFstrBase + 33)
#define ISER_RDMA_MULTICAST_FAILED          (iserFstrBase + 34)
#define ISER_RDMA_EVENT_HANDLER             (iserFstrBase + 35)
#define ISER_RDMA_BIND_ADDR_SUCCESS         (iserFstrBase + 36)
#define ISER_RDMA_RSVL_ADDR_SUCCESS         (iserFstrBase + 37)

#define ISER_CREATE_FLOW_FAILED             (iserFstrBase + 38)
#define ISER_CREATE_FLOW_SUCCESS            (iserFstrBase + 39)

#define ISER_IO_POST_RECEIVE                (iserIoFstrBase + 0)
#define ISER_IO_POST_SEND                   (iserIoFstrBase + 1)
#define ISER_IO_RECEIVE_COMP                (iserIoFstrBase + 2)
#define ISER_IO_IPV4_ARP                    (iserIoFstrBase + 3)
#define ISER_IO_IPV6_ARP                    (iserIoFstrBase + 4)
#define ISER_IO_IPV4_ICMP_ECHO_REQ          (iserIoFstrBase + 5)
#define ISER_IO_IPV4_ICMP_ECHO_REPLY        (iserIoFstrBase + 6)
#define ISER_IO_IPV4_ICMP_UNKNOWN           (iserIoFstrBase + 7)
#define ISER_IO_IPV4_TCP                    (iserIoFstrBase + 8)
#define ISER_IO_SEND_COMP                   (iserIoFstrBase + 9)

/***  LOCALS  ***/

/***  PROCEDURES  ***/


/**
 * This function initializes the iser ArpIcmp dq
 *
 * %4B list in host format %4E in network format
 */
void iserArpIcmpInitDq
    (
    )
{
    const char* tag = "iserArp";

    iserFstrBase = dqvkiNextFsn(iserDqWriter);
//  v-d v- start "       v start text
    dqvkiAddFstr(iserDqWriter, ISER_ARP_START, FDL_NORMAL, tag,
        "%h iserArp      ARP responder arpStart called for port %d, m_QP 0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_ARP_PING_NOT_ATTACHED, FDL_NORMAL, tag,
        "%h iserArp      PingV4 failed, ARP Responder not Attached to Multicast group.");
    dqvkiAddFstr(iserDqWriter, ISER_ARP_PINGV6_NOT_ATTACHED, FDL_NORMAL, tag,
        "%h iserArp      PingV6 failed, ARP Responder not Attached to Multicast group.");
    dqvkiAddFstr(iserDqWriter, ISER_CREATE_QP, FDL_NORMAL, tag,
        "%h iserArp      Create QP Port %d Status %s");
    dqvkiAddFstr(iserDqWriter, ISER_CLEANUP_QP, FDL_NORMAL, tag,
        "%h iserArp      Cleanup QP Port %d, m_QP 0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_POST_RECEIVE_FAILED, FDL_NORMAL, tag,
        "%h iserArp      Port Receive Error Port %d idx %d Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_POST_SEND_ERROR, FDL_NORMAL, tag,
        "%h iserArp      Post Send Error Port %d idx %d Error %d wr\n%4B");
    dqvkiAddFstr(iserDqWriter, ISER_RECEIVE_BUF, FDL_DETAIL, tag,
        "%h iserArp      Receive buffer Port %d idx %d\n%4E");
    dqvkiAddFstr(iserDqWriter, ISER_SEND_BUF, FDL_DETAIL, tag,
        "%h iserArp      Send buffer Port %d idx %d\n%4E");
    dqvkiAddFstr(iserDqWriter, ISER_SEND_COMP_ERROR, FDL_NORMAL, tag,
        "%h iserArp      Send buffer Port %d idx %d Status %d\n%4B");
    dqvkiAddFstr(iserDqWriter, ISER_MULT_ATTACH, FDL_NORMAL, tag,
        "%h iserArp      arpMulticastAttach: Multicast Attach port %d start");
    dqvkiAddFstr(iserDqWriter, ISER_MULT_JOIN_ERROR, FDL_NORMAL, tag,
        "%h iserArp      arpMulticastAttach: ib_sa_join_multicast error: port %d, P_key 0x%x, err: %d");
    dqvkiAddFstr(iserDqWriter, ISER_MULT_JOIN_RET, FDL_NORMAL, tag,
        "%h iserArp      arpMulticastAttach: ib_sa_join_multicast successful: port %d P_Key 0x%x SaMulticast:0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_MULT_JOIN_COMP, FDL_NORMAL, tag,
        "%h iserArp      multicastJoinComplete: port %d returned status %d m_QP 0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_QP_MODIFY_ERROR, FDL_HI_PRI, tag,
        "%h iserArp      QP modify error port %d QP 0x%x status %d %s");
    dqvkiAddFstr(iserDqWriter, ISER_QP_STATE, FDL_NORMAL, tag,
        "%h iserArp      QP cycle state port %d QP 0x%x QKey 0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_QP_READY, FDL_NORMAL, tag,
        "%h iserArp      QP is ready: port %d QP 0x%x QKey 0x%x");
    dqvkiAddFstr(iserDqWriter, ISER_ATTACH_MULTICAST_ERROR, FDL_HI_PRI, tag,
        "%h iserArp      ib_attach_mcast error port %d, QP 0x%x, mgid 0x%x, mlid 0x%x, status %d");
    dqvkiAddFstr(iserDqWriter, ISER_TIMER_START, FDL_NORMAL, tag,
        "%h iserArp      mcastTimerStart: port %d, Running %s, Enabled %s, delay %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARPICMP_SM_CHG, FDL_NORMAL, tag,
        "%h iserArp      SM Changed: port %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARPICMP_PORT_CHG, FDL_NORMAL, tag,
        "%h iserArp      Port Changed: port %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARPICMP_PORT_CHG_QUEUE, FDL_NORMAL, tag,
        "%h iserArp      Port Changed: Queueing multicast attach: port %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARPICMP_PKEY_CHG, FDL_NORMAL, tag,
        "%h iserArp      P_Key Changed: port %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARP_START_QUEUE, FDL_NORMAL, tag,
        "%h iserArp      arpStart: QP created, queue multicast attach: port %d, m_QP 0x%x, delay %d");
    dqvkiAddFstr(iserDqWriter, ISER_MULT_JOIN_RETRY, FDL_NORMAL, tag,
        "%h iserArp      multicastJoinRetry: Queueing Retry: port %d, delay %d, retry %d");
    dqvkiAddFstr(iserDqWriter, ISER_ARPICMP_EVENT, FDL_NORMAL, tag,
        "%h iserArp      eventHandler: Got event %d on port %d, event port %d");
    dqvkiAddFstr(iserDqWriter, ISER_TIMER_STOP, FDL_NORMAL, tag,
        "%h iserArp      mcastTimerStop: port %d, Running %s, Enabled %s");
    dqvkiAddFstr(iserDqWriter, ISER_TIMER_ENABLE, FDL_NORMAL, tag,
        "%h iserArp      mcastTimerEnable: port %d, Running %s, Enabled %s");
    dqvkiAddFstr(iserDqWriter, ISER_TIMER_DISABLE, FDL_NORMAL, tag,
        "%h iserArp      mcastTimerDisable: port %d, Running %s, Enabled %s");
    dqvkiAddFstr(iserDqWriter, ISER_CLEANUP_QP_DONE, FDL_NORMAL, tag,
        "%h iserArp      Cleanup QP Successful IB Port %d, m_QP 0x%x");

    dqvkiAddFstr(iserDqWriter, ISER_RDMA_BIND_ADDR_FAILED, FDL_NORMAL, tag,
        "%h iserArp      rdma_bind_addr Failed port %d, Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_RSVL_ADDR_FAILED, FDL_NORMAL, tag,
        "%h iserArp      rdma_resolve_addr Failed port %d, Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_RSVL_ROUT_FAILED, FDL_NORMAL, tag,
        "%h iserArp      rdma_resolve_route Failed port %d, Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_MULTICAST_FAILED, FDL_NORMAL, tag,
        "%h iserArp      rdma_join_multicast Failed port %d, Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_EVENT_HANDLER, FDL_NORMAL, tag,
        "%h iserArp      rdma_cm_event handler port %d, Event %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_BIND_ADDR_SUCCESS, FDL_NORMAL, tag,
        "%h iserArp      rdma_bind_addr Success port %d");
    dqvkiAddFstr(iserDqWriter, ISER_RDMA_RSVL_ADDR_SUCCESS, FDL_NORMAL, tag,
        "%h iserArp      rdma_resolve_addr Success port %d");

    dqvkiAddFstr(iserDqWriter, ISER_CREATE_FLOW_FAILED, FDL_NORMAL, tag,
        "%h iserArp      ib_create_flow Failed port %d, Error %d");
    dqvkiAddFstr(iserDqWriter, ISER_CREATE_FLOW_SUCCESS, FDL_NORMAL, tag,
        "%h iserArp      ib_create_flow Success port %d");

    iserIoFstrBase = dqvkiNextFsn(iserIoDqWriter);
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_POST_RECEIVE, FDL_NORMAL, tag,
        "%h iserArp      Post Receive Buffer Port %d Idx %d");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_POST_SEND, FDL_DETAIL, tag,
        "%h iserArp      Post Send Port %d Idx %d QP 0x%x length 0x%x buf 0x%x, WR\n%4B");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_RECEIVE_COMP, FDL_DETAIL, tag,
        "%h iserArp      Receive Buffer Port %d Idx %d FrameType 0x%x data 0x%x length 0x%x wc\n%4B");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV4_ARP, FDL_NORMAL, tag,
        "%h iserArp      IPv4 Arp Request Port %d %I");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV6_ARP, FDL_NORMAL, tag,
        "%h iserArp      IPv6 Arp Request Port %d %2E");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV4_ICMP_ECHO_REQ, FDL_NORMAL, tag,
        "%h iserArp      IPv4 ICMP ECHO REQ Port %d IPv4 checksum 0x%x ICMP checksum 0x%x");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV4_ICMP_ECHO_REPLY, FDL_NORMAL, tag,
        "%h iserArp      IPv4 ICMP ECHO REPLY Port %d IPv4 checksum 0x%x ICMP checksum 0x%x");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV4_ICMP_UNKNOWN, FDL_NORMAL, tag,
        "%h iserArp      IPv4 ICMP ECHO REPLY Port %d Type 0x%x IPv4 checksum 0x%x ICMP checksum 0x%x");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_IPV4_TCP, FDL_DETAIL, tag,
        "%h iserArp      IPv4 TCP Port %d IPv4 checksum 0x%x TCP checksum 0x%x Header\n%4E");
    dqvkiAddFstr(iserIoDqWriter, ISER_IO_SEND_COMP, FDL_DETAIL, tag,
        "%h iserArp      Receive Buffer Port %d Idx %d wc\n%4B");

}


/**
 * Constructor for ArpIcmp
 *
 */
iser::ArpIcmp::ArpIcmp
    (
    MtlInterface * intf,
    NetDevice *    netDevice
    )
    :
    m_Intf(intf),
    m_NetDevice(netDevice),
    m_QP(NULL),
    m_PD(NULL),
    m_RxCQ(NULL),
    m_TxCQ(NULL),
    m_MR(NULL),
    m_QKey(0),
    m_McastDelay(1),
    m_McastRetry(0),
    m_McastDelayWork(intf->getMtl()->getWorkqueue(), this, &ArpIcmp::mcastRetry),
    m_SMChanged(intf->getMtl()->getWorkqueue(), this, &ArpIcmp::SMChanged),
    m_PortChanged(intf->getMtl()->getWorkqueue(), this, &ArpIcmp::PortChanged),
    m_PkeyChanged(intf->getMtl()->getWorkqueue(), this, &ArpIcmp::PkeyChanged),
    m_McastTimerRunning(false),
    m_McastTimerEnabled(true),
    m_McastJoinBusy(false),
    m_MulticastAttached(false),
    m_remoteQPN(0),
    m_flow(NULL)
{
    // Allocate Data buffer
    // Need to allocate addition buffers for ARP_REQ_NUM_BUF ARP Requests
    m_Buffer = static_cast<BYTE *>(VKI_SMZALLOC((ARP_ICMP_TOTAL_NUM_BUF)*ARP_BUFFER_SIZE, VKI_KMNOSLEEP));
    assert(m_Buffer != NULL);   // Nothing to do if this fails

    // Set the start of each RX/TX buffer
    for (int i = 0; i < ARP_ICMP_TOTAL_NUM_BUF; i++)
    {
        m_DataBuf[i] = m_Buffer + (i*ARP_BUFFER_SIZE);
        m_Ah[i] = NULL;         // Clear the Address Handle
    }

    // Initialize ARP Request data
    // We reserve the last RX/TX buffer
    m_ArpRequestIdx = ARP_ICMP_NUM_BUF;

    // Create semaphore to wait on ARP responses (for our ARP Requests)
    m_ActiveArpRequest = VKI_BSEM_INIT(SEM_Q_FIFO, SEM_EMPTY);
    assert(m_ActiveArpRequest != NULL);

    // We have members to track active ARP Requests.  NULL indicates no active requests
    VKI_MEMCLEAR(&m_ActiveIPv4Addr, IPv4_LENGTH);
    VKI_MEMCLEAR(&m_ActiveIPv6Addr, IPv4_LENGTH);
    VKI_MEMCLEAR(&m_remoteGID, IB_HW_ADDR_LEN);

    // Initialize ArpEventHandler member.
    // It's members will be set during arpStart.
    VKI_MEMCLEAR(&m_ArpEventHandler, sizeof(struct ib_event_handler));
}

/**
 * Destructor for ArpIcmp
 *
 */
iser::ArpIcmp::~ArpIcmp
    (
    )
{
    arpCleanupQP();

    // Free any buffers we have
    if (m_Buffer)
        VKI_SMFREE(m_Buffer);

    for (int i = 0; i < ARP_ICMP_TOTAL_NUM_BUF; i++)
    {
        if (m_Ah[i] != NULL)
            ib_destroy_ah(m_Ah[i]);     // release the Address Handle
    }
}

/**
 * Get the ArpIcmp object reference for an async event.
 *
 * \param handler - should be the m_ArpEventHandler member
 *
 * \returns the ArpIcmp object containing the given member
 */
iser::ArpIcmp*
iser::ArpIcmp::getArpIcmpFromEventHandler(struct ib_event_handler* handler)
{
    return OBJECT_FROM_MEMBER(ArpIcmp, m_ArpEventHandler, handler);
}

/**
 * ArpIcmp Event handler
 *
 * \param handler
 * \param event
 */
void arpEventHandler(struct ib_event_handler * handler, struct ib_event * event)
{
    // Pull the arpIcmp object reference out of the handler reference.
    iser::ArpIcmp * arpIcmp = iser::ArpIcmp::getArpIcmpFromEventHandler(handler);
    arpIcmp->eventHandler(event);
}

/**
 * Called to start this ARP responder on this port
 *
 *
 * \return bool - Started correctly
 */
bool
iser::ArpIcmp::arpStart
    (
    )
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_ARP_START, getIbPortNumber(), m_QP);

    // Prepare event handler
    // The device should be valid at this time.
    m_ArpEventHandler.device = getIbDevice();
    m_ArpEventHandler.handler = arpEventHandler;

    // Register Event Handler
    ib_register_event_handler  (&m_ArpEventHandler);

    // Start timer loop to join the multicast group
    mcastTimerStart(1);
    return true;
}

//
//  Forward defines to set these functions a "C"
extern "C"
void arpReceiveCompletionCallback(struct ib_cq *cq, void *cq_context);

extern "C"
void arpSendCompletionCallback(struct ib_cq *cq, void *cq_context);

extern "C"
void arpEventCallback(struct ib_event *event, void *cq_context);


/**
 * Create the Queue Pair
 *
 * This also transition the QP to active/RTS
 *
 * \return bool - Creation worked
 */
bool
iser::ArpIcmp::arpCreateQP
    (
    )
{
    struct ib_qp_init_attr qpInitAttr;

    // Create Protection Domain
    m_PD = ib_alloc_pd(getIbDevice());
    if(IS_ERR(m_PD))
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "PD alloc failed");
        m_PD = NULL;
        return false;
    }

    // Create Completions queue cq
    m_RxCQ = ib_create_cq(getIbDevice(), arpReceiveCompletionCallback, arpEventCallback, this, ARP_ICMP_TOTAL_NUM_BUF, 0);
    if(IS_ERR(m_RxCQ))
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "Receive CQ Create failed");
        m_RxCQ = NULL;
        return false;
    }

    // Create Completions queue cq
    m_TxCQ = ib_create_cq(getIbDevice(), arpSendCompletionCallback, arpEventCallback, this, ARP_ICMP_TOTAL_NUM_BUF, 0);
    if(IS_ERR(m_TxCQ))
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "Send CQ Create failed");
        m_TxCQ = NULL;
        return false;
    }

    // Request Notify on both CQ's
    int ret = ib_req_notify_cq(m_RxCQ, IB_CQ_NEXT_COMP);
    if (ret)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "Receive Notify CQ failed");
        return false;
    }

    ret = ib_req_notify_cq(m_TxCQ, IB_CQ_NEXT_COMP);
    if (ret) {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "Send Notify CQ failed");
        return false;
    }

    //  Get a memory region
    m_MR = ib_get_dma_mr(m_PD, (IB_ACCESS_REMOTE_WRITE | IB_ACCESS_LOCAL_WRITE | IB_ACCESS_REMOTE_READ));
    if(IS_ERR(m_MR))
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_CREATE_QP, getIbPortNumber(), "get dma MR failed");
        m_MR = NULL;
        return false;
    }

    //    Setup up the QP Initial attribute
    VKI_MEMCLEAR(&qpInitAttr, sizeof(qpInitAttr));

    qpInitAttr.qp_type            = IB_QPT_UD;
    qpInitAttr.sq_sig_type        = IB_SIGNAL_ALL_WR;
    qpInitAttr.qp_context         = this;
    qpInitAttr.send_cq            = m_TxCQ;
    qpInitAttr.recv_cq            = m_RxCQ;
    qpInitAttr.srq                = NULL;
    qpInitAttr.cap.max_send_wr    = ARP_ICMP_TOTAL_NUM_BUF;
    qpInitAttr.cap.max_recv_wr    = ARP_ICMP_TOTAL_NUM_BUF;
    qpInitAttr.cap.max_send_sge   = 1;
    qpInitAttr.cap.max_recv_sge   = 1;
    qpInitAttr.cap.max_inline_data = 0;
    qpInitAttr.port_num           = getIbPortNumber();

    m_QP = ib_create_qp(m_PD, &qpInitAttr);
    if(IS_ERR(m_QP))
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
            ISER_CREATE_QP, getIbPortNumber(), "QP Create Failed");

        m_QP = 0;
        return false;
    }


    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_CREATE_QP, getIbPortNumber(), "SUCCESS");

    return true;
}

/**
 * Delete the resources associated with this Queue Pair
 */
void
iser::ArpIcmp::arpCleanupQP
    (
    )
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_CLEANUP_QP, getIbPortNumber(), m_QP);

    if(m_flow) {
      ib_destroy_flow(m_flow);
      m_flow = NULL;
    }

    if (m_MR)
    {
        ib_dereg_mr(m_MR);
        m_MR = NULL;
    }

    if (m_TxCQ)
    {
        ib_destroy_cq(m_TxCQ);
        m_TxCQ = NULL;
    }

    if (m_RxCQ)
    {
        ib_destroy_cq(m_RxCQ);
        m_RxCQ = NULL;
    }

    if (m_PD)
    {
        ib_dealloc_pd(m_PD);
        m_PD = NULL;
    }

    if (m_QP)
    {
        ib_destroy_qp(m_QP);
        m_QP = NULL;
    }

    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_CLEANUP_QP_DONE, getIbPortNumber(), m_QP);
}

/**
 * Post a receive buffer
 *
 *
 * \param idx = Buffer Index
 *
 * \return bool - Post succeeded
 */
bool
iser::ArpIcmp::postReceive(int idx)
{
    struct ib_recv_wr rr;
    struct ib_sge sge;
    struct ib_recv_wr *bad_wr;
    int rc;

    dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(),
        ISER_IO_POST_RECEIVE, getIbPortNumber(), idx);

    /* prepare the scatter/gather entry */
    VKI_MEMCLEAR(&sge, sizeof(sge));

    sge.addr    =  virtualPtrToPhysicalCaddr(m_DataBuf[idx]);
    sge.length  = ARP_BUFFER_SIZE;
    sge.lkey    = m_MR->lkey;

    /* prepare the RR */
    VKI_MEMCLEAR(&rr, sizeof(rr));

    rr.next     = NULL;
    rr.wr_id    = idx;
    rr.sg_list  = &sge;
    rr.num_sge  = 1;

    /* post the Receive Request to the RQ */
    rc = ib_post_recv(m_QP, &rr, &bad_wr);
    if (rc)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
            ISER_POST_RECEIVE_FAILED, getIbPortNumber(), idx, rc);

        return false;
    }

    return true;
}

/**
 * Receive completion callback
 *
 * Converts from C to C++ context
 *
 * \param cq - completing CQ
 * \param cq_context Completing CQ context (C++ ArpIcmp)
 */
void arpReceiveCompletionCallback
    (
    struct ib_cq *cq,
    void *cq_context
    )
{
    iser::ArpIcmp * arpIcmp = reinterpret_cast<iser::ArpIcmp*>(cq_context);
    arpIcmp->receiveCompletionCallback(cq);
}

/**
 * C++ Receive Completion callback.
 *
 * Poll for any work and process the received buffer
 *
 * This could be
 * an IPv4 ARP request
 * an IPv6 ARP request
 * an Echo request
 *
 *
 * \param cq
 */
void
iser::ArpIcmp::receiveCompletionCallback
    (
    struct ib_cq *cq
    )
{
    /* this cb indicates at least one completion */
    struct ib_wc        wc;

    while(ib_poll_cq(cq, 1, &wc) == 1)
    {
        int     idx = static_cast<int>(wc.wr_id);
        UINT32  remoteQP;
        BYTE *  data = m_DataBuf[idx] + sizeof(struct ib_grh);
        UINT32  dataLength = wc.byte_len - sizeof(struct ib_grh);

        struct FrameHdr      * frameHdr = (struct FrameHdr * )(data);
        bool    frameProcessed = false;

        /* check completion and repost */

        //  The receive buffer starts with a frame header with the type
        //  Branch on that type
        dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_RECEIVE_COMP, getIbPortNumber(),
            idx, ntohs(frameHdr->frameType), data, dataLength, sizeof(struct ib_wc), &wc);

        if (iserArpDqBufLength)
        {
            dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_RECEIVE_BUF,
                getIbPortNumber(), idx, min(dataLength,(UINT32)iserArpDqBufLength), data);
        }

        switch (ntohs(frameHdr->frameType))
        {
            //  ARP request or ARP response.
            case ARP_FRAME_TYPE:
            {
                frameProcessed = arpFrame(&wc, data, dataLength);
                break;
            }

            case IPv4_FRAME_TYPE:
            {
                //  Switch on protocol
                struct IPv4Packet *ipv4Hdr = (struct IPv4Packet *)(data + sizeof(struct FrameHdr));
                UINT32      headerLength = (ipv4Hdr->versionLength & 0x0F) * 4;
                BYTE *      ipv4Start    = data + sizeof(struct FrameHdr) + headerLength;
                UINT32      ipv4Length   = ntohs(ipv4Hdr->totalLength) - headerLength;


                switch (ipv4Hdr->protocol)
                {
                    case PROTOCOL_ICMP:
                    {
                        struct IcmpHdr * icmpHdr = (struct IcmpHdr *)(ipv4Start);
                        //
                        //  Switch on ICMP protocol
                        //
                        switch (icmpHdr->icmpType)
                        {
                            case ECHO_REQUEST:
                            {
                                //  Swap the source and destination address.
                                //  This does not affect an additive checksum
                                UINT32 temp         = ipv4Hdr->sourceAddr;
                                ipv4Hdr->sourceAddr = ipv4Hdr->destAddr;
                                ipv4Hdr->destAddr   = temp;

                                //  Compute the 16 bit checksum over this message.  The checksum is magic in that it doesn't require
                                //  network bit order to make it work.
                                UINT32  icmpLength = dataLength - sizeof(struct FrameHdr) - sizeof(IPv4Packet);

                                dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_IPV4_ICMP_ECHO_REQ, getIbPortNumber(),
                                checksum((UINT16*)ipv4Hdr, headerLength), checksum((UINT16 *)icmpHdr, icmpLength));

                                icmpHdr->icmpType = ECHO_REPLY;
                                icmpHdr->checksum = 0;              // preset for computation
                                icmpHdr->checksum = checksum((UINT16 *)icmpHdr, icmpLength);

                                // create AH from WC - use the same index as buf
                                wc.wc_flags &= ~IB_WC_GRH;      // clear the GRH in order to generate non grh AH for the reply
                                m_Ah[idx] = ib_create_ah_from_wc(m_PD, &wc, (struct ib_grh*)(m_DataBuf[idx]), getIbPortNumber());

                                if (m_Ah[idx] == NULL)
                                {
                                    VKI_CMN_ERR(CE_ERROR, " %s failed to create ah", __func__);
                                }

                                // Remote queue pair from WC request.
                                remoteQP = wc.src_qp;
                                postSend(idx, data, dataLength, remoteQP);

                                frameProcessed = true;
                                break;
                            }
                            case ECHO_REPLY:
                            {
                                //  Compute the 16 bit checksum over this message.  The checksum is magic in that it doesn't require
                                //  network bit order to make it work.
                                UINT32  icmpLength = dataLength - sizeof(struct FrameHdr) - sizeof(IPv4Packet);

                                dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_IPV4_ICMP_ECHO_REQ, getIbPortNumber(),
                                checksum((UINT16*)ipv4Hdr, headerLength), checksum((UINT16 *)icmpHdr, icmpLength));

                                break;
                            }
                            //
                            //  Other Icmp Type here
                            //
                            default:
                            {
                                //  Compute the 16 bit checksum over this message.  The checksum is magic in that it doesn't require
                                //  network bit order to make it work.
                                UINT32  icmpLength = dataLength - sizeof(struct FrameHdr) - sizeof(IPv4Packet);

                                dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_IPV4_ICMP_UNKNOWN, getIbPortNumber(),
                                    icmpHdr->icmpType, checksum((UINT16*)ipv4Hdr, headerLength), checksum((UINT16 *)icmpHdr, icmpLength));

                                break;
                            }
                        }
                        break;
                    }
                    case PROTOCOL_TCP:
                    {
                        struct TCPPacket * tcp = (struct TCPPacket *)ipv4Start;

                        //
                        //  Build pseudo header
                        struct TCPPseudoHdr   pHdr;
                        pHdr.sourceAddr = ipv4Hdr->sourceAddr;
                        pHdr.destAddr = ipv4Hdr->destAddr;
                        pHdr.zero = 0;
                        pHdr.protocol = ipv4Hdr->protocol;
                        pHdr.totalLength = htons(ipv4Length);

                        // Compute header checksum fragment
                        UINT16  pHdrChecksum = ~checksum((UINT16*)&pHdr, sizeof(struct TCPPseudoHdr));
                        UINT16  dataChecksum = ~checksum((UINT16 *)ipv4Start, ipv4Length);
                        UINT32  partChecksum = pHdrChecksum + dataChecksum;
                        partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF);
                        partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF); // handle second level overflows
                        UINT16  finalChecksum = ~(UINT16)partChecksum;

                        dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_IPV4_TCP, getIbPortNumber(),
                            checksum((UINT16*)ipv4Hdr, headerLength), finalChecksum, sizeof(struct TCPPacket), tcp);

                        //  Swap the source and destination address.
                        //  This does not affect an additive checksum
                        UINT32      tempAddr = ipv4Hdr->sourceAddr;
                        ipv4Hdr->sourceAddr = ipv4Hdr->destAddr;
                        ipv4Hdr->destAddr   = tempAddr;

                        // and ports
                        UINT16 temp = tcp->sourcePort;
                        tcp->sourcePort = tcp->destPort;
                        tcp->destPort = temp;
                        UINT16 options = ntohs(tcp->options);

                        // Check for open session
                        if ((options & (TCP_SYN | TCP_ACK)) == TCP_SYN)
                        {
                            VKI_CMN_ERR(CE_NOTE, "receive SYN, responding with SYC ACK");
                            UINT32  seqNo = ntohl(tcp->seqNo);
                            seqNo += 1;                         // Bump Seq No
                            tcp->ackNo = htonl(seqNo);          //
                            tcp->seqNo = htonl(tcp->seqNo + 0x0102);  // Random number
                            options |= TCP_ACK;                 // Add ACK, leave hdr length
                            tcp->options = htons(options);
                            tcp->checksum = 0;
                            dataChecksum = ~checksum((UINT16 *)ipv4Start, ipv4Length);
                            partChecksum = pHdrChecksum + dataChecksum;
                            partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF);
                            partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF); // handle second level overflows
                            tcp->checksum =  ~(UINT16)partChecksum;     // Assume even bytes
                            VKI_MEM_DISPLAY(ipv4Start, ipv4Length/4,4);

                            dataChecksum = ~checksum((UINT16 *)ipv4Start, ipv4Length);
                            partChecksum = pHdrChecksum + dataChecksum;
                            partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF);
                            partChecksum = (partChecksum & 0xFFFF) + ((partChecksum >> 16) & 0xFFFF); // handle second level overflows
                            finalChecksum = ~(UINT16)partChecksum;

                            VKI_CMN_ERR(CE_NOTE, "TCP sent Checksum %x", finalChecksum);

                            // create AH from WC - use the same index as buf
                            wc.wc_flags &= ~IB_WC_GRH;      // clear the GRH in order to generate non grh AH for the replay
                            m_Ah[idx] = ib_create_ah_from_wc(m_PD, &wc, (struct ib_grh*)(m_DataBuf[idx]), getIbPortNumber());

                            if (m_Ah[idx] == NULL)
                            {
                                VKI_CMN_ERR(CE_ERROR, " %s failed to create ah", __func__);
                            }

                            // Remote queue pair from WC request.
                            remoteQP = wc.src_qp;
                            postSend(idx, data, dataLength, remoteQP);

                            frameProcessed = true;
                            break;

                        }
                        else if ((options & (TCP_SYN | TCP_ACK)) == TCP_ACK)
                        {
                            VKI_CMN_ERR(CE_NOTE, "TCP ACK response");
                            break;
                        }
                        else
                        {
                            // Throw away the ACK we get back
                            break;
                        }
                    }

                    //
                    //  Other IPv4 protocols here
                    //
                    default:
                        break;
                }
                break;
            }

            case IPv6_FRAME_TYPE:
                VKI_CMN_ERR(CE_NOTE,"IPv6 Frame arrived and was dropped");
                break;

            //
            //  Other Frame Types here
            //
            default:
                break;
        }

        //
        //  See if we handled the frame or not
        //
        if (!frameProcessed)
        {
            postReceive(idx);
        }
    }

    int ret = ib_req_notify_cq(m_RxCQ, IB_CQ_NEXT_COMP);
    if (ret)
    {
        VKI_CMN_ERR(CE_ERROR, "Request Notify CQ failed");
    }
}


/**
 * Process an ARP frame
 *
 * If it is for us and we process it we return true.  If not we
 * return false and the caller reposts the receive buffer
 *
 * \param wc - current work request
 *
 * \param pktStart - This start with the frame header to make
 *        our work easier.  I.e. we have the whole frame to turn
 *        around to the sender.
 *
 * \param pktLength - Total length of the packet.  We return the
 *        packed based on the packet size not this value.
 *
 *
 * \return bool - We send a response.
 */
bool
iser::ArpIcmp::arpFrame
    (
    struct ib_wc  *  wc,
    BYTE * pktStart,
    UINT32 pktLength
    )
{
    BYTE    IPv4Addr[IPv4_LENGTH];
    BYTE    IPv6Addr[IPv6_LENGTH];
    BYTE    senderIPv4Addr[IPv4_LENGTH];
    BYTE    senderIPv6Addr[IPv6_LENGTH];
    UINT32  remoteQP;
    int     idx = static_cast<int>(wc->wr_id);

    //
    //  Check for quest or response
    //
    struct IPv4ArpPacket * ipv4Ptr = (struct IPv4ArpPacket * )(pktStart);
    struct IPv6ArpPacket * ipv6Ptr = (struct IPv6ArpPacket * )(pktStart);

    switch (ntohs(ipv4Ptr->op))
    {
        case ARPOP_REQUEST:
        {
            if (isIPv4ArpRequest(ipv4Ptr, IPv4Addr))
            {
                UINT32  addr4dq = *(UINT32 *)IPv4Addr;
                addr4dq = ntohl(addr4dq);

                dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(),
                    ISER_IO_IPV4_ARP, getIbPortNumber(), addr4dq);

                //  Map socket structure to address field
                struct sockaddr_in sockin;
                VKI_MEMCPY(&sockin, m_NetDevice->getIPv4Address()->addr, sizeof(struct sockaddr_in));

                //  Check if this is for us.
                if (VKI_MEMCMP(IPv4Addr, &sockin.sin_addr.s_addr, IPv4_LENGTH) != 0)
                {
                    return false;
                }

                remoteQP = ntohl(ipv4Ptr->sourceQPN) & QPN_MASK;  // Remote queue pair from ARP request.

                // create AH from WC - use the same index as buf
                wc->wc_flags     &= ~IB_WC_GRH;      // clear the GRH in order to generate non grh AH for the reply
                m_Ah[idx]       = ib_create_ah_from_wc(m_PD, wc, (struct ib_grh*)(m_DataBuf[idx]), getIbPortNumber());

                if (IS_ERR(m_Ah[idx]))
                {
                    VKI_CMN_ERR(CE_ERROR, " %s failed to create ah error %d", __func__, m_Ah[idx]);
                    m_Ah[idx] = NULL;
                    assert(false);      // JFF FIXME
                    return false;
                }

                // Create the ARP response and send it back
                arpIPv4CreateResponse(ipv4Ptr, IPv4Addr);
                postSend(idx, ipv4Ptr, sizeof(struct IPv4ArpPacket), remoteQP);

                return true;
            }
            else if (isIPv6ArpRequest(ipv6Ptr, IPv6Addr))
            {
                dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(),
                    ISER_IO_IPV6_ARP, getIbPortNumber(), 16, IPv6Addr);

                bool        found = false;
                struct sockaddr_in6 sockin;

                //  Check if this is for us.
                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6LinkLocalAddress()->addr, sizeof(struct sockaddr_in6));
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6Routable0Address()->addr, sizeof(struct sockaddr_in6));
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6Routable1Address()->addr, sizeof(struct sockaddr_in6));
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                if (!found)
                    return false;

                remoteQP = ntohl(ipv6Ptr->sourceQPN) & QPN_MASK;      // Remote queue pair from ARP request.

                // create AH from WC - use the same index as buf
                wc->wc_flags &= ~IB_WC_GRH;      // clear the GRH in order to generate non grh AH for the reply
                m_Ah[idx] = ib_create_ah_from_wc(m_PD, wc, (struct ib_grh*)(m_DataBuf[idx]), getIbPortNumber());

                if (m_Ah[idx] == NULL)
                {
                    VKI_CMN_ERR(CE_ERROR, " %s failed to create ah", __func__);
                }

                // Create the ARP response and send it back
                arpIPv6CreateResponse(ipv6Ptr, IPv6Addr);
                postSend(idx, ipv6Ptr, sizeof(struct IPv6ArpPacket), remoteQP);

                return true;
            }
            break;
        }

        case ARPOP_REPLY:
        {
            if (isIPv4ArpReply(ipv4Ptr, IPv4Addr, senderIPv4Addr))
            {
                //  Map socket structure to address field
                struct sockaddr_in sockin;
                VKI_MEMCPY(&sockin, m_NetDevice->getIPv4Address()->addr, sizeof(struct sockaddr_in));

                //  Check if we are the target and it matches outstanding IPv4 ARP request
                if ((VKI_MEMCMP(IPv4Addr, &sockin.sin_addr.s_addr, IPv4_LENGTH) == 0) &&
                    (VKI_MEMCMP(senderIPv4Addr, m_ActiveIPv4Addr, IPv4_LENGTH) == 0))
                {
                    // Save remote QPN and GID for display by piing command
                    m_remoteQPN = ipv4Ptr->sourceQPN;
                    VKI_MEMCPY(m_remoteGID, ipv4Ptr->sourceGID, IB_HW_ADDR_LEN);

                    // Wake up ARP Request task to indicate success
                    VKI_BSEM_GIVE(m_ActiveArpRequest);
                }
                return true;
            }
            else if (isIPv6ArpReply(ipv6Ptr, IPv6Addr, senderIPv6Addr))
            {
                bool        found = false;
                struct sockaddr_in6 sockin;

                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6LinkLocalAddress()->addr, sizeof(struct sockaddr_in6));

                //  Check if any of this ports IPv6 addresses are the target IP
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6Routable0Address()->addr, sizeof(struct sockaddr_in6));
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                VKI_MEMCPY(&sockin, m_NetDevice->getIPv6Routable1Address()->addr, sizeof(struct sockaddr_in6));
                if (VKI_MEMCMP(IPv6Addr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH) == 0)
                    found = true;

                // If the target IPv6 is one of ours, check if matches the outstanding IPv6 ARP
                if (found && (VKI_MEMCMP(senderIPv6Addr, m_ActiveIPv6Addr, IPv6_LENGTH) == 0))
                {
                    // Save remote QPN and GID for display by piing command
                    m_remoteQPN = ipv6Ptr->sourceQPN;
                    VKI_MEMCPY(m_remoteGID, ipv6Ptr->sourceGID, IB_HW_ADDR_LEN);

                    // Wake up ARP Request task to indicate success
                    VKI_BSEM_GIVE(m_ActiveArpRequest);
                }
                return true;
            }
            break;
        }

        default:
            break;
    }   // End switch on Op Type

    //
    //  We didn't process this request so return false
    //
    return false;
}

/**
 * Post a send request
 *
 * NOTE: The Address Handle (ib_ah) must be setup for this
 * request.
 *
 * \param idx - buffer AH index
 * \param buf - Data to send
 * \param length - Length to send
 * \param remoteQP - Remove Queue Pair to send this to
 *
 * \return bool
 */
bool
iser::ArpIcmp::postSend
    (
    int idx,
    void * buf,
    int length,
    UINT32  remoteQP
    )
{
    struct ib_send_wr sr;
    struct ib_sge sge;
    struct ib_send_wr *bad_wr;
    int rc;


    /* prepare the scatter/gather entry */
    VKI_MEMCLEAR(&sge, sizeof(sge));

    sge.addr    = virtualPtrToPhysicalCaddr(buf);
    sge.length  = length;
    sge.lkey    = m_MR->lkey;

    /* prepare the sr */
    VKI_MEMCLEAR(&sr, sizeof(sr));

    sr.next     = NULL;
    sr.wr_id    = idx;
    sr.sg_list  = &sge;
    sr.num_sge  = 1;
    sr.opcode   = IB_WR_SEND;

    sr.wr.ud.ah = m_Ah[idx];
    sr.wr.ud.remote_qpn = remoteQP;
    sr.wr.ud.remote_qkey = ntohl(m_QKey);

    /* post the Receive Request to the RQ */
    dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_POST_SEND, getIbPortNumber(),
        idx, remoteQP, length, buf, sizeof(struct ib_send_wr), &sr);

    if (iserArpDqBufLength)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_SEND_BUF,
            getIbPortNumber(), idx, min(length,iserArpDqBufLength), buf);
    }

    rc = ib_post_send(m_QP, &sr, &bad_wr);
    if (rc)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_POST_SEND_ERROR,
            getIbPortNumber(), idx, rc, sizeof(struct ib_send_wr), &sr);

        return false;
    }

    return true;
}

/**
 * C to C++ transition callback for a Send Complete
 *
 *
 * \param cq
 * \param cq_context
 */
void arpSendCompletionCallback
    (
    struct ib_cq *cq,
    void *cq_context
    )
{
    iser::ArpIcmp * arpIcmp = reinterpret_cast<iser::ArpIcmp*>(cq_context);
    arpIcmp->sendCompletionCallback(cq);
}

/**
 * Send Complete callback.
 *
 * For now all this does is find the requests and release the
 * address handle.
 *
 * For ARP this will have to process the response.
 *
 *
 * \param cq
 */
void
iser::ArpIcmp::sendCompletionCallback
    (
    struct ib_cq *cq
    )
{
    struct ib_wc wc;

    while(ib_poll_cq(cq, 1, &wc) == 1)
    {
        int     idx = static_cast<int>(wc.wr_id);

        dqvkiWriteWithId(iserIoDqWriter, getChannelNumber(), ISER_IO_SEND_COMP,
            getIbPortNumber(), idx, sizeof(struct ib_wc), &wc);

        if (m_Ah[idx] != NULL)
        {
            ib_destroy_ah(m_Ah[idx]);
            m_Ah[idx] = NULL;
        }

        if (wc.status != 0)
        {
            dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_SEND_COMP_ERROR,
                getIbPortNumber(), idx, wc.status, sizeof(struct ib_wc), &wc);
        }

        postReceive(idx);
    }

    int ret = ib_req_notify_cq(m_TxCQ, IB_CQ_NEXT_COMP);
    if (ret)
    {
        VKI_CMN_ERR(CE_ERROR, "Request Notify CQ failed");
    }
}

/**
 * C to C++ connector for Event Callback
 *
 *
 * \param event
 * \param cq_context
 */
void arpEventCallback(struct ib_event *event, void *cq_context)
{
    iser::ArpIcmp * arpIcmp = reinterpret_cast<iser::ArpIcmp*>(cq_context);
    arpIcmp->eventHandler(event);
}

/**
 * This is called when any event arrives for this ib_device.
 *
 * It is handed off to the proper interface.  For non=port
 * unique events, the are handed off to all interfaces.
 *
 * \param event
 */
void
iser::ArpIcmp::eventHandler(ibEvent * event)
{

    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_ARPICMP_EVENT,
        event->event, getIbPortNumber(), event->element.port_num);

    //  Makesure it is for us
    if (event->element.port_num != getIbPortNumber())
        return;

    //  Handle events
    switch (event->event)
    {
        // Events using the Comnpletion Queue Event Layout
        case IB_EVENT_CQ_ERR:
            // Should not see this event.
            // This event is handled by the CQ event handler.
            break;

        // Events using the Queue Pair Event Layout
        // Not handled here
        case IB_EVENT_QP_FATAL:
        case IB_EVENT_QP_REQ_ERR:
        case IB_EVENT_QP_ACCESS_ERR:
        case IB_EVENT_COMM_EST:
        case IB_EVENT_SQ_DRAINED:
        case IB_EVENT_PATH_MIG:
        case IB_EVENT_PATH_MIG_ERR:
        case IB_EVENT_QP_LAST_WQE_REACHED:
            break;

        // Events using the Shared Receive Queue Event Layout
        // Not handled here
        case IB_EVENT_SRQ_LIMIT_REACHED:
        case IB_EVENT_SRQ_ERR:
            break;

        // Events using the Port State Change Event Layout
        // IB_EVENT_PORT_ERR - Link has gone inactive.
        // m_Intf's Link state and SM "Ready" status are updated by iser::AsyncEvents.
        // m_NetDevice's m_NetDeviceV4 and m_NetDeviceV6 GID based
        // devAddrs are also ultimately updated by iser::AsyncEvents
        // via the processAddressStateChange() call.
        //
        // Disable the Multicast timer so we do not attemp to the join multicast
        // group while the port is down.
        case IB_EVENT_PORT_ERR:
            mcastTimerDisable();
            break;

        // IB_EVENT_PORT_ACTIVE - Link has gone active.
        // m_Intf's GID table and Link state are updated by iser::AsyncEvents.
        // m_NetDevice's m_NetDeviceV4 and m_NetDeviceV6 GID based
        // devAddrs are also ultimately updated by iser::AsyncEvents
        // via the processAddressStateChange() call.
        //
        // Enable the Multicast timer to restart attemps to join the multicast group.
        case IB_EVENT_PORT_ACTIVE:
            mcastTimerEnable();
            mcastTimerStart(1);
            break;

        case IB_EVENT_LID_CHANGE:
        {
            //
            //  update state and start the workqueue for port status
            m_PortChanged.queue();
            break;
        }

        case IB_EVENT_PKEY_CHANGE:
        {
            // Start the workqueue for PKey changed
            m_PkeyChanged.queue();
            break;
        }

        case IB_EVENT_CLIENT_REREGISTER:
        case IB_EVENT_SM_CHANGE:
        {
            // Session manager changed we need to re-offer
            m_SMChanged.queue();
            break;
        }

        case IB_EVENT_GID_CHANGE:
        {
            // The SM has updated this port's GID.
            // The m_Intf's GLID table is updated by iser::AsyncEvent.
            // The iser::NetDevice's addrs are updated by iser::AsyncEvent.
            // Nothing else to do.
            break;
        }

        // Generic events
        case IB_EVENT_DEVICE_FATAL:
        case IB_EVENT_INVALID:
        default:
            VKI_CMN_ERR(CE_ERROR, "ArpIcmp Event: Unknown Event %d", event->event);
            break;
    }
}


/**
 * Create and IPv4 ARP response from and ARP request
 *
 * Reverse the send and receive addresses
 * Make the destination QP = source QP
 * Add out QP and GID
 *
 *
 * \param arpPkt - ARP Packet
 * \param d_qpn - Our QP
 * \param gid - Returned GID
 * \param ipAddr - IPv4 address
 */
void
iser::ArpIcmp::arpIPv4CreateResponse
    (
    struct IPv4ArpPacket * arpPkt,
    BYTE ipAddr[IPv4_LENGTH]
    )
{
    // Convert to reply
    arpPkt->op = htons(ARPOP_REPLY); // change it to reply

    //copy source to dst/target
    arpPkt->targetQPN = arpPkt->sourceQPN;
    VKI_MEMCPY(arpPkt->targetGID, arpPkt->sourceGID, IB_HW_ADDR_LEN);
    VKI_MEMCPY(arpPkt->targetIpAddr, arpPkt->senderIpAddr, IPv4_LENGTH);

    // set the source
    arpPkt->sourceQPN = htonl(m_QP->qp_num);
    VKI_MEMCPY(arpPkt->sourceGID, m_Intf->getGID(0)->raw, IB_HW_ADDR_LEN);
    VKI_MEMCPY(arpPkt->senderIpAddr, ipAddr, IPv4_LENGTH);
}

/**
 * Create and IPv6 ARP response from and ARP request
 *
 * Reverse the send and receive addresses
 * Make the destination QP = source QP
 * Add out QP and GID
 *
 *
 * \param arpPkt - ARP Packet
 * \param d_qpn - Our QP
 * \param gid - Returned GID
 * \param ipAddr - IPv6 address
 */
void
iser::ArpIcmp::arpIPv6CreateResponse
    (
    struct IPv6ArpPacket * arpPkt,
    BYTE ipAddr[IPv6_LENGTH]
    )
{
    // Convert to reply
    arpPkt->op = htons(ARPOP_REPLY); // change it to reply

    //copy source to dst/target
    arpPkt->targetQPN = arpPkt->sourceQPN;
    VKI_MEMCPY(arpPkt->targetGID, arpPkt->sourceGID, IB_HW_ADDR_LEN);
    VKI_MEMCPY(arpPkt->targetIpAddr, arpPkt->senderIpAddr, IPv6_LENGTH);

    // set the source
    arpPkt->sourceQPN = htonl(m_QP->qp_num);
    VKI_MEMCPY(arpPkt->sourceGID, arpPkt->targetGID, IB_HW_ADDR_LEN);
    VKI_MEMCPY(arpPkt->senderIpAddr, ipAddr, IPv6_LENGTH);
}

/**
 * Create an ARP request Address Handle
 *
 * \param - none
 *
 * \returns and IB address handle suitable to send an ARP request
 */
struct ib_ah *
iser::ArpIcmp::create_ah_for_arp
    (
    )
{
    struct ib_ah_attr ah_attr;

    // ah_attr.grh.dgid          = m_rdmaId->route.path_rec->dgid;
    // ah_attr.grh.flow_label    = htonl(m_rdmaId->route.path_rec->flow_label);
    // ah_attr.grh.hop_limit     = m_rdmaId->route.path_rec->hop_limit;
    // ah_attr.grh.sgid_index    = 0;
    // ah_attr.grh.traffic_class = m_rdmaId->route.path_rec->traffic_class;
    //
    // ah_attr.dlid        = htons(m_rdmaId->route.path_rec->dlid);
    // ah_attr.sl          = m_rdmaId->route.path_rec->sl;
    // ah_attr.static_rate = m_rdmaId->route.path_rec->rate;
    // ah_attr.ah_flags    = IB_AH_GRH;
    // ah_attr.port_num    = getIbPortNumber();

    // Create Address Handle with given attributes
    struct ib_ah * ah = ib_create_ah(m_PD, &ah_attr);

    return ah;
}

/**
 * Create and IPv4 ARP request
 *
 * \param destIpAddr - destination IPv4 address
 * \param arpPkt - ARP Packet
 */
void
iser::ArpIcmp::arpIPv4CreateRequest
    (
    BYTE destIpAddr[IPv4_LENGTH],
    struct IPv4ArpPacket * arpPkt
    )
{
    // Clear buffer, targetGID will remain zeroed out.
    VKI_MEMCLEAR(arpPkt, sizeof(struct IPv4ArpPacket));

    arpPkt->header.frameType = ntohs(ARP_FRAME_TYPE);
    arpPkt->hwType = ntohs(IB_HW_TYPE);
    arpPkt->protType = ntohs(IP_PROTO_TYPE);
    arpPkt->hwAddrSize = INFINIBAND_ALEN;
    arpPkt->protAddrSize = IPv4_LENGTH;
    arpPkt->op = ntohs(ARPOP_REQUEST);

    // Set the source QPN and GID
    arpPkt->sourceQPN = htonl(m_QP->qp_num);
    VKI_MEMCPY(arpPkt->sourceGID, m_Intf->getGID(0)->raw, IB_HW_ADDR_LEN);

    // Use target QPN of 0xffffff for ARP
    arpPkt->targetQPN = 0xffffff;

    // Set the sender IP address
    // Map socket structure to address field
    struct sockaddr_in sockin;
    VKI_MEMCPY(&sockin, m_NetDevice->getIPv4Address()->addr, sizeof(struct sockaddr_in));
    VKI_MEMCPY(arpPkt->senderIpAddr, &sockin.sin_addr.s_addr, IPv4_LENGTH);

    // Set the target IP address
    VKI_MEMCPY(arpPkt->targetIpAddr, destIpAddr, IPv4_LENGTH);
}

/**
 * Create and IPv6 ARP request
 *
 * \param destIpAddr - destination IPv4 address
 * \param arpPkt - ARP Packet
 */
void
iser::ArpIcmp::arpIPv6CreateRequest
    (
    BYTE destIpAddr[IPv6_LENGTH],
    struct IPv6ArpPacket * arpPkt
    )
{
    // Clear buffer, targetGID will remain zeroed out.
    VKI_MEMCLEAR(arpPkt, sizeof(struct IPv6ArpPacket));

    arpPkt->header.frameType = ntohs(ARP_FRAME_TYPE);
    arpPkt->hwType = ntohs(IB_HW_TYPE);
    arpPkt->protType = ntohs(IP_PROTO_TYPE);
    arpPkt->hwAddrSize = INFINIBAND_ALEN;
    arpPkt->protAddrSize = IPv6_LENGTH;
    arpPkt->op = ntohs(ARPOP_REQUEST);

    // Set the source QPN and GID
    arpPkt->sourceQPN = htonl(m_QP->qp_num);
    VKI_MEMCPY(arpPkt->sourceGID, m_Intf->getGID(0)->raw, IB_HW_ADDR_LEN);

    // Set the sender IP address
    // Map socket structure to address field
    struct sockaddr_in6 sockin;
    VKI_MEMCPY(&sockin, m_NetDevice->getIPv6LinkLocalAddress()->addr, sizeof(struct sockaddr_in6));
    VKI_MEMCPY(arpPkt->senderIpAddr, sockin.sin6_addr.in6.addr8, IPv6_LENGTH);

    // Use target QPN of 0xffffff for ARP
    arpPkt->targetQPN = 0xffffff;

    // Set the target IP address
    VKI_MEMCPY(arpPkt->targetIpAddr, destIpAddr, IPv6_LENGTH);
}

/**
 * Create the IPv4 Broadcast GID.  This is the same as the
 * net_device broadcast ID without the PQ.
 */
void
iser::ArpIcmp::createIPv4BroadcastGID
    (
        union ib_gid *mgid
    )
{
    // Get out PKEY
    UINT16  pkey = getIntf()->ibGetDefaultPKey();

    // Build the MGID
    mgid->raw[0] = 0xff;
    mgid->raw[1] = 0x12;
    mgid->raw[2] = 0x40;
    mgid->raw[3] = 0x1b;
    mgid->raw[4] = (uint8_t)(pkey & 0xff00 >> 8);
    mgid->raw[5] = (uint8_t)(pkey & 0x00ff);
    mgid->raw[6] = 0;
    mgid->raw[7] = 0;
    mgid->raw[8] = 0;
    mgid->raw[9] = 0;
    mgid->raw[10] = 0;
    mgid->raw[11] = 0;
    mgid->raw[12] = 0xff;
    mgid->raw[13] = 0xff;
    mgid->raw[14] = 0xff;
    mgid->raw[15] = 0xff;
}

/**
 * This enables the multicast timer.
 *
 * \param None
 */
void
iser::ArpIcmp::mcastTimerEnable
    (
    )
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_TIMER_ENABLE, getIbPortNumber(),
        m_McastTimerRunning ? "True" : "False", m_McastTimerEnabled ? "True" : "False");

    // Enable future timer starts
    m_McastTimerEnabled = true;
}

/**
 *
 * This disables the multicast timer.
 *
 * \param None
 *
 * This cancels the timer if it running. This is for handling
 * errors where we need to stop operation early.
 *
 */
void
iser::ArpIcmp::mcastTimerDisable()
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_TIMER_DISABLE, getIbPortNumber(),
        m_McastTimerRunning ? "True" : "False", m_McastTimerEnabled ? "True" : "False");

    // Disable future timer starts and reset delay time
    m_McastTimerEnabled = false;
    m_McastDelay = 1;

    // Stop the timer if running
    mcastTimerStop();
}

/**
 * We want to delay for "delay" seconds and then try/retry the
 * multicast join
 *
 * This starts the timer if it not running.
 *
 * \param delay in seconds
 */
void
iser::ArpIcmp::mcastTimerStart
    (
    unsigned long delay
    )
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_TIMER_START, getIbPortNumber(),
        m_McastTimerRunning ? "True" : "False", m_McastTimerEnabled ? "True" : "False", delay);

    if(m_McastTimerEnabled)
    {
        if (!m_McastTimerRunning)
        {
            m_McastDelayWork.queue(delay*60); // 60x to get seconds
            m_McastTimerRunning = true;
        }
    }
}

/**
 *
 * We want to delay for "delay" seconds and then try/retry the
 * multicast join
 *
 * This cancels the timer if it running. This is for handling
 * errors where we need to stop operation early.
 *
 */
void
iser::ArpIcmp::mcastTimerStop()
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_TIMER_STOP, getIbPortNumber(),
        m_McastTimerRunning ? "True" : "False", m_McastTimerEnabled ? "True" : "False");

    if (m_McastTimerRunning)
    {
        m_McastDelayWork.cancel();
        m_McastTimerRunning = false;
    }
}

/**
 * Try/Retry the multicast join.
 */
void
iser::ArpIcmp::mcastRetry()
{
    m_McastTimerRunning = false;
    m_McastRetry++;

    if(!arpMulticastInit()) {
      m_McastDelay *= 2;
      if(m_McastDelay > McastMaxDelay)
        m_McastDelay = McastMaxDelay;

      mcastTimerStart(m_McastDelay);
    }
}

/**
 * Process Session manager changed events
 *
 * Currently executed for:
 * IB_EVENT_CLIENT_REREGISTER - SM requests port to re-register for all subscriptions.
 *                              m_Intf's SM "Ready" status is updated by iser::AsyncEvents.
 *                              m_NetDevice's m_NetDeviceV4 and m_NetDeviceV6 GID based
 *                              devAddrs are also ultimately updated by iser::AsyncEvents
 *                              via the processAddressStateChange() call.
 * IB_EVENT_SM_CHANGE - The SM being used on this port has changed.
 *
 */
void
iser::ArpIcmp::SMChanged()
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_ARPICMP_SM_CHG, getIbPortNumber());

    // Currently we get the "arpMcastJoinComplete" callback call that automatically
    // attempts to Attach to the multicast group, destroying the old QP, creating
    // a new QP, cycling the new QP to the RTS state and performing the QP join operation.

    // There doesn't appear to be anything else required as long as the "arpMcastJoinComplete"
    // callback is reliable.
}

/**
 * Process port state change events
 *
 * Currently executed for:
 * IB_EVENT_LID_CHANGE - The LID has changed on this port.
 *                       m_Intf's LID is updated by iser::AsyncEvents.
 *
 * Based on the sub-net Manager Ready status, this member will either unregister/detach
 * or register/attach.
 */
void
iser::ArpIcmp::PortChanged()
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_ARPICMP_PORT_CHG, getIbPortNumber());

    // Currently we get the "arpMcastJoinComplete" callback call that automatically
    // attempts to Attach to the multicast group, destroying the old QP, creating
    // a new QP, cycling the new QP to the RTS state and performing the QP join operation.

    // There doesn't appear to be anything else required as long as the "arpMcastJoinComplete"
    // callback is reliable.
}

/**
 * Process Pkey Changed event.
 *
 * Currently executed for:
 * IB_EVENT_PKEY_CHANGE - The P_Key (Partition Key) table has changed on this port/device
 *                        by the SM.  The GID table indexes may have changed.
 *                        m_Intf's GID table is updated by iser::AsyncEvents.
 *                        m_NetDevice's m_NetDeviceV4 and m_NetDeviceV6 GID based
 *                        devAddrs are also ultimately updated by iser::AsyncEvents
 *                        via the processAddressStateChange() call.
 *
 * The ARP Responder always uses the default (index 0) partition key, however since the GID
 * table indexes may have changed, this member function will re-join the multicast group.
 */
void
iser::ArpIcmp::PkeyChanged()
{
    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_ARPICMP_PKEY_CHG, getIbPortNumber());

    mcastTimerStart(1);
}

/**
 * Initiate a Multicast Attach operation
 *
 * The goal here is to attach to the Multicast group for ARP
 * requests.
 *
 * \return bool
 */
bool
iser::ArpIcmp:: arpMulticastInit
    (
    )
{
    struct ib_flow_attr *flow_attr;
    struct ib_flow_spec *spec_info;
    int spec_size;

    // create the QP
    if (m_QP) {
        arpCleanupQP();
    }
    if(!arpCreateQP()) {
        return false;
    }

    // calculate attr size
    spec_size = sizeof(struct ib_flow_attr)
      + sizeof(struct ib_flow_spec_eth);

    // allocate spec
    flow_attr = VKI_PMZALLOC(spec_size, VKI_KMNOSLEEP);
    VKI_MEMCLEAR(flow_attr, spec_size);

    // apply flow rules
    flow_attr->type = IB_FLOW_ATTR_MC_DEFAULT;
    flow_attr->priority = 0;
    flow_attr->flags = 0;
    flow_attr->port = getIbPortNumber();
    flow_attr->num_of_specs = 1;
    flow_attr->size = spec_size;

    // setup eth spec
    spec_info = (struct ib_flow_spec*)(((void *)flow_attr) + sizeof(struct ib_flow_attr));
    spec_info->eth.type = IBV_FLOW_SPEC_ETH;
    spec_info->eth.size = sizeof(struct ib_flow_spec_eth);
    spec_info->eth.val.ether_type = 0;
    VKI_MEMSET(spec_info->eth.mask.dst_mac, 0, sizeof(spec_info->eth.mask.src_mac));

    // create flow object
    m_flow = ib_create_flow(m_QP, flow_attr, 0);
    if(!m_flow) {
      dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
          ISER_CREATE_FLOW_FAILED, getIbPortNumber(), m_flow);
      return false;
    } else {
      dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
          ISER_CREATE_FLOW_SUCCESS, getIbPortNumber());
    }

    return true;
}

/**
 * Cycle the QP state to RTS.
 *
 * \param None
 *
 * \return int - 0 if success, 1 if failure
 */
int
iser::ArpIcmp::cycleQPState
    (
    )
{
    //  Save the host order QKey
    //m_QKey = ntohl(rdmaEvent->param.ud.qkey);

    dqvkiWriteWithId(iserDqWriter, getChannelNumber(),
        ISER_QP_STATE, getIbPortNumber(), m_QP, m_QKey);

    /* Per IB spec, we have to transition through states to ready to send */
    struct ib_qp_attr qpAttr;
    VKI_MEMCLEAR((char*)&qpAttr, sizeof(qpAttr));

    /* RESET->INIT */
    qpAttr.qp_state     = IB_QPS_INIT;
    qpAttr.pkey_index   = 0;
    qpAttr.port_num     = getIbPortNumber();
    qpAttr.qkey         = m_QKey;
    int r = ib_modify_qp(m_QP, &qpAttr,
        IB_QP_STATE | IB_QP_PKEY_INDEX |
        IB_QP_PORT | IB_QP_QKEY);

    if(r)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_QP_MODIFY_ERROR,
            getIbPortNumber(), m_QP, r, "QP -> Init failed");

        return 1;           // Caller will re-queue sequence.
    }

    // Transition INIT->RTR
    VKI_MEMCLEAR((char*)&qpAttr, sizeof(qpAttr));

    qpAttr.qp_state = IB_QPS_RTR;

    r = ib_modify_qp(m_QP, &qpAttr, IB_QP_STATE);
    if(r)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_QP_MODIFY_ERROR, getIbPortNumber(),
                         m_QP, r, "QP -> RTR failed");
        return 1;           // Caller will re-queue sequence.
    }

    // Transition RTR->RTS
    VKI_MEMCLEAR((char*)&qpAttr, sizeof(qpAttr));

    qpAttr.qp_state = IB_QPS_RTS;
    qpAttr.sq_psn = 0;

    r = ib_modify_qp(m_QP, &qpAttr, IB_QP_STATE | IB_QP_SQ_PSN);
    if(r)
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_QP_MODIFY_ERROR,
                         getIbPortNumber(), m_QP, r, "QP -> RTS failed");
        return 1;           // Caller will re-queue sequence.
    }

    dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_QP_READY,
                     getIbPortNumber(), m_QP, m_QKey);

    for (int i = 0; i < ARP_ICMP_TOTAL_NUM_BUF; i++)
        postReceive(i);

    return 0;
}

/**
 * See if this is an IPv4 Arp request
 *
 *
 * \param ib_pkt - Possible ARP request
 * \param ipAddr - returned IPv4 address if this is an IPv4 ARP
 *
 * \return bool - True is ARP request.
 */
bool
iser::ArpIcmp::isIPv4ArpRequest
    (
    struct IPv4ArpPacket * ib_pkt,
    BYTE * ipAddr
    )
{
    /* ensure the packet is a valid ARP request */
    if ((ib_pkt->hwType != htons(IB_HW_TYPE)) ||
        (ib_pkt->protType != htons(IP_PROTO_TYPE)) ||
        (ib_pkt->hwAddrSize != INFINIBAND_ALEN) ||  // 20 byte;
        (ib_pkt->protAddrSize != IPv4_LENGTH ))
    {

        return false;
    }

    VKI_MEMCPY(ipAddr, ib_pkt->targetIpAddr,IPv4_LENGTH);

    return true;
}

/**
 * See if this is an IPv6 Arp request
 *
 *
 * \param ib_pkt - Possible ARP request
 * \param ipAddr - returned IPv4 address if this is an IPv4 ARP
 *
 * \return bool - True is ARP request.
 */
bool
iser::ArpIcmp::isIPv6ArpRequest
    (
    struct IPv6ArpPacket * ib_pkt,
    BYTE * ipAddr
    )
{
    /* ensure the packet is a valid ARP request */
    if ((ib_pkt->header.frameType != htons(ARP_FRAME_TYPE)) ||
        (ib_pkt->hwType != htons(IB_HW_TYPE)) ||
        (ib_pkt->protType != htons(IP_PROTO_TYPE)) ||
        (ib_pkt->hwAddrSize != 0x14) ||  // 20 byte;
        (ib_pkt->protAddrSize != IPv6_LENGTH ))
    {
        return false;
    }

    VKI_MEMCPY(ipAddr, ib_pkt->targetIpAddr,IPv6_LENGTH);

    return true;
}

/**
 * See if this is an IPv4 Arp reply
 *
 *
 * \param ib_pkt - Possible ARP reply
 * \param targetIpAddr - returned target IPv4 address if this is an IPv4 ARP Reply
 * \param senderIpAddr - returned sender IPv4 address if this is an IPv4 ARP Reply
 *
 * \return bool - True is ARP reply.
 */
bool
iser::ArpIcmp::isIPv4ArpReply
    (
    struct IPv4ArpPacket * ib_pkt,
    BYTE * targetIpAddr,
    BYTE * senderIpAddr
    )
{
    /* ensure the packet is a valid ARP request */
    if ((ib_pkt->header.frameType != htons(ARP_FRAME_TYPE)) ||
        (ib_pkt->hwType != htons(IB_HW_TYPE)) ||
        (ib_pkt->protType != htons(IP_PROTO_TYPE)) ||
        (ib_pkt->hwAddrSize != INFINIBAND_ALEN) ||  // 20 byte;
        (ib_pkt->protAddrSize != IPv4_LENGTH )) {

        return false;
    }

    // Return the target and sender IPs
    VKI_MEMCPY(targetIpAddr, ib_pkt->targetIpAddr,IPv4_LENGTH);
    VKI_MEMCPY(senderIpAddr, ib_pkt->senderIpAddr,IPv4_LENGTH);

    return true;
}

/**
 * See if this is an IPv6 Arp reply
 *
 *
 * \param ib_pkt - Possible ARP reply
 * \param targetIpAddr - returned target IPv6 address if this is an IPv6 ARP Reply
 * \param senderIpAddr - returned sender IPv6 address if this is an IPv6 ARP Reply
 *
 * \return bool - True is ARP reply.
 */
bool
iser::ArpIcmp::isIPv6ArpReply
    (
    struct IPv6ArpPacket * ib_pkt,
    BYTE * targetIpAddr,
    BYTE * senderIpAddr
    )
{
    /* ensure the packet is a valid ARP request */
    if ((ib_pkt->header.frameType != htons(ARP_FRAME_TYPE)) ||
        (ib_pkt->hwType != htons(IB_HW_TYPE)) ||
        (ib_pkt->protType != htons(IP_PROTO_TYPE)) ||
        (ib_pkt->hwAddrSize != 0x14) ||  // 20 byte;
        (ib_pkt->protAddrSize != IPv6_LENGTH )) {

        return false;
    }

    // Return the target and sender IPs
    VKI_MEMCPY(targetIpAddr, ib_pkt->targetIpAddr,IPv6_LENGTH);
    VKI_MEMCPY(senderIpAddr, ib_pkt->senderIpAddr,IPv6_LENGTH);

    return true;
}

/**
 * Send an IPv4 ARP request
 *
 * \param addr - Given IPv4 Address to send ARP request
 * \return bool - True if an ARP request sent and a response was received.
 *                Otherwise false.
 */
bool
iser::ArpIcmp::sendArpRequest
    (
    BYTE *   addr,
    UINT32 * remoteQPN,
    BYTE * remoteGID
    )
{
    // Default return code is false
    bool rc = false;

    // Use reserved ARP Request buffer space for ARP packet
    VKI_MEMCLEAR(m_DataBuf[m_ArpRequestIdx], ARP_BUFFER_SIZE);
    struct IPv4ArpPacket * arpPkt = (struct IPv4ArpPacket *) m_DataBuf[m_ArpRequestIdx];

    // If the ARP Responder is not attached to the multicast group, return fail.
    if( !m_MulticastAttached )
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_ARP_PING_NOT_ATTACHED);
        return rc;
    }

    // Create an Address Handle for the arp request
    m_Ah[m_ArpRequestIdx] = create_ah_for_arp();
    if (IS_ERR(m_Ah[m_ArpRequestIdx]))
    {
        VKI_CMN_ERR(CE_ERROR, "%s: Failed to create Address Handle: error %d", __func__,
            m_Ah[m_ArpRequestIdx]);
        m_Ah[m_ArpRequestIdx] = NULL;
        return rc;
    }

    // Create an IPv4 ARP request for the given IPv4 address
    arpIPv4CreateRequest(addr, arpPkt);

    // Indicate an active IPv4 ARP request is outstanding
    VKI_MEMCPY(m_ActiveIPv4Addr, addr, IPv4_LENGTH);

    // Send the ARP request to the multicast group
    // Use 0xffffff for remote Queue Pair Number
    postSend(m_ArpRequestIdx, arpPkt, sizeof(struct IPv4ArpPacket), 0xffffff);

    // If a valid ARP response is received, return true.
    STATUS stat = VKI_BSEM_TAKE(m_ActiveArpRequest, ISER_ARP_TIMEOUT);
    if(stat != OK && errno != S_objLib_OBJ_TIMEOUT)
    {
        VKI_CMN_ERR(CE_ERROR, "%s: Semaphore Error %d", __func__, errno);
    }
    else if(errno == S_objLib_OBJ_TIMEOUT)
    {
        VKI_CMN_ERR(CE_NOTE, "%s: ARP timeout.", __func__);
    }
    else
    {
        // Successfully received an ARP response
        // Save remote QPN and GID for display by piing command
        *remoteQPN = m_remoteQPN;
        VKI_MEMCPY(remoteGID, m_remoteGID, IB_HW_ADDR_LEN);

        rc = true;
    }

    // Indicate there is no longer an outstanding IPv4 ARP request
    VKI_MEMCLEAR(m_ActiveIPv4Addr, IPv4_LENGTH);
    return rc;
}


/**
 * Send an IPv6 ARP request
 *
 * \param addr - Given IPv6 Address to send ARP request
 * \return bool - True if an IPv6 ARP request sent and a response was received.
 *                Otherwise false.
 */
bool
iser::ArpIcmp::sendArpV6Request
    (
    BYTE *   addr,
    UINT32 * remoteQPN,
    BYTE * remoteGID
    )
{
    // Default return code is false
    bool rc = false;

    // Use reserved ARP Request buffer space for ARP packet
    VKI_MEMCLEAR(m_DataBuf[m_ArpRequestIdx], ARP_BUFFER_SIZE);
    struct IPv6ArpPacket * arpPkt = (struct IPv6ArpPacket *) m_DataBuf[m_ArpRequestIdx];

    // If the ARP Responder is not attached to the multicast group, return fail.
    if( !m_MulticastAttached )
    {
        dqvkiWriteWithId(iserDqWriter, getChannelNumber(), ISER_ARP_PINGV6_NOT_ATTACHED);
        return rc;
    }

    // Create an Address Handle for the arp request
    m_Ah[m_ArpRequestIdx] = create_ah_for_arp();
    if (IS_ERR(m_Ah[m_ArpRequestIdx]))
    {
        VKI_CMN_ERR(CE_ERROR, "%s: Failed to create Address Handle: error %d", __func__,
            m_Ah[m_ArpRequestIdx]);
        m_Ah[m_ArpRequestIdx] = NULL;
        return rc;
    }

    // Create an IPv6 ARP request for the given IPv6 address
    arpIPv6CreateRequest(addr, arpPkt);

    // Indicate an active IPv6 ARP request is outstanding
    VKI_MEMCPY(m_ActiveIPv6Addr, addr, IPv6_LENGTH);

    // Send the ARP request to the multicast group
    // Use 0xffffff for remote Queue Pair Number
    postSend(m_ArpRequestIdx, arpPkt, sizeof(struct IPv6ArpPacket), 0xffffff);

    // If a valid ARP response is received, return true.
    STATUS stat = VKI_BSEM_TAKE(m_ActiveArpRequest, ISER_ARP_TIMEOUT);
    if(stat != OK && errno != S_objLib_OBJ_TIMEOUT)
    {
        VKI_CMN_ERR(CE_ERROR, "%s: Semaphore Error %d", __func__, errno);
    }
    else if(errno == S_objLib_OBJ_TIMEOUT)
    {
        VKI_CMN_ERR(CE_NOTE, "%s: ARP timeout.", __func__);
    }
    else
    {
        // Successfully received an ARP response
        // Save remote QPN and GID for display by piing command
        *remoteQPN = m_remoteQPN;
        VKI_MEMCPY(remoteGID, m_remoteGID, IB_HW_ADDR_LEN);
        rc = true;
    }

    // Indicate there is no longer an outstanding IPv6 ARP request
    VKI_MEMCLEAR(m_ActiveIPv6Addr, IPv6_LENGTH);
    return rc;
}

//
//  This general area is for Workqueue's
//
//  Basically we derive our classes from Work.  This allows us to queue work to
//  a different thread.  Events that arrive can't cause ib_verbs actions because
//  of locking.  So we move off to our own thread.
//
//  In addition, and really the requirment for this code, we can queue work for
//  a later time.  This allows for retrying the multicast join.
//

/**
 * This is the description of a method to be called on the
 * Workqueue task.
 *
 *
 * \param workQ - Workqueue to use.  Ours is created by the MTL
 * \param data - This is the Class part of the Class->Method
 *        call.
 * \param func - This is the method part of the Class->Method
 *        call.
 */
iser::ArpWork::ArpWork
    (
    Workqueue *     workQ,
    ArpIcmp *       data,
    ArpIcmpWork func
    )
    :
    Work(workQ),
    m_Class(data),
    m_WorkMethod(func)
{
}

/**
 * Never called, but if it was we would cancel anything active
 * and go away.
 *
 */
iser::ArpWork::~ArpWork()
{
    cancel();
}

/**
 * This is the shim to actually call the Class->Method
 *
 */
void
iser::ArpWork::doWork()
{
    (m_Class->*m_WorkMethod)();
}

/**
 * Return what data we have.
 *
 *
 * \return const char*
 */
const char*
iser::ArpWork::toString() const
{
    return utlStaticStringSprintf("%s ", Work::toString());
}
/**
 * This is the description of a method to be called on the
 * Workqueue task. In this case it is always called after a
 * delay
 *
 *
 * \param workQ - Workqueue to use.  Ours is created by the MTL
 * \param data - This is the Class part of the Class->Method
 *        call.
 * \param func - This is the method part of the Class->Method
 *        call.
 *
 */
iser::ArpDelayedWork::ArpDelayedWork
    (
    Workqueue * workQ,
    ArpIcmp *   data,
    ArpIcmpWork func
    )
    :
    DelayedWork(workQ),
    m_Class(data),
    m_WorkMethod(func)
{
}

/**
 * Destructor
 *
 * Cancel any outstanding work
 *
 */
iser::ArpDelayedWork::~ArpDelayedWork()
{
    cancel();
}

/**
 * This is the shim to actually call the Class->Method
 */
void
iser::ArpDelayedWork::doWork
    (
    )
{
    (m_Class->*m_WorkMethod)();
}

/**
 * Return what data we have.
 *
 *
 * \return const char*
 */
const char*
iser::ArpDelayedWork::toString() const
{
    return utlStaticStringSprintf("%s ", DelayedWork::toString());
}

/**
 * Prints iser ArpIcmp object details. This is a
 * utl::ShowableObject overridden function.
 */
void
iser::ArpIcmp::showDetails
    (
    ) const
{
    VKI_PRINTF ("iser:ArpIcmp............... 0x%08x (sz: %u)\n", this, sizeof(*this));
    VKI_PRINTF ("  Intf..................... 0x%08x\n", m_Intf);
    VKI_PRINTF ("  IbPortNumber............. 0x%08x\n", m_Intf ? getIbPortNumber() : -1);
    VKI_PRINTF ("  ChannelNumber............ 0x%08x\n", m_Intf ? getChannelNumber() : -1);
    VKI_PRINTF ("  NetDevice................ 0x%08x\n", m_NetDevice);
    VKI_PRINTF ("  QP....................... 0x%08x\n", m_QP);
    VKI_PRINTF ("  PD....................... 0x%08x\n", m_PD);
    VKI_PRINTF ("  RxCQ..................... 0x%08x\n", m_RxCQ);
    VKI_PRINTF ("  TxCQ..................... 0x%08x\n", m_TxCQ);
    VKI_PRINTF ("  MR....................... 0x%08x\n", m_MR);
    VKI_PRINTF ("  Qkey..................... 0x%08x\n", m_QKey);
    VKI_PRINTF ("  Buffer................... 0x%08x\n", m_Buffer);
    VKI_PRINTF ("  Databuf Array Start...... 0x%08x\n", m_DataBuf);
    VKI_PRINTF ("  Ah Array start........... 0x%08x\n", m_Ah);
    VKI_PRINTF ("  McastDelay .............. %d\n", m_McastDelay);
    VKI_PRINTF ("  McastRetry .............. %d\n", m_McastRetry);
    VKI_PRINTF ("  McastDelayWork........... 0x%08x\n", &m_McastDelayWork);
    VKI_PRINTF ("      %s\n", m_McastDelayWork.toString());
    VKI_PRINTF ("  SMChanged................ 0x%08x\n", &m_SMChanged);
    VKI_PRINTF ("      %s\n", m_SMChanged.toString());
    VKI_PRINTF ("  PortChanged.............. 0x%08x\n", &m_PortChanged);
    VKI_PRINTF ("      %s\n", m_PortChanged.toString());
    VKI_PRINTF ("  PkeyChanged.............. 0x%08x\n", &m_PkeyChanged);
    VKI_PRINTF ("      %s\n", m_PkeyChanged.toString());
    VKI_PRINTF ("  McastTimerRunning........ %s\n", m_McastTimerRunning ? "True" : "False");
    VKI_PRINTF ("  McastTimerEnabled........ %s\n", m_McastTimerEnabled ? "True" : "False");
    VKI_PRINTF ("  McastJoinBusy............ %s\n", m_McastJoinBusy ? "True" : "False");
    VKI_PRINTF ("  MulticastAttached........ %s\n", m_MulticastAttached ? "True" : "False");
}
